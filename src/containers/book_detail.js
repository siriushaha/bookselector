import React, { Component } from 'react';
import { connect } from 'react-redux';
import { activeBook } from '../actions/index';
import { bindActionCreators } from 'redux';

class BookDetail extends Component {
    
    render() {
        const book = this.props.book;
        if (!book)
            return <div> Select a book  </div>
            
        return (
            <div>
                <h3>Details for:</h3>
                <div>{book.title}</div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    // Whatever is returned will show up as props in BookList
    return {
        book: state.activeBook
    }
}

//Anything returned from this will end up as props on BookList container
//function mapDispatchToProps(dispatch) {
    // Whener selectBook is called, the result should be passed to all of our reducers
//    return bindActionCreators({ selectBook: selectBook}, dispatch);
//}

// Promote BookList from a component to a container - it needs to know about this new dispatch method selectBook. Make it available as a prop
export default connect(mapStateToProps)(BookDetail);