import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';

class BookList extends Component {
    
    renderList(books) {
        return books.map((book, index) => {
            return (
                <li 
                    key={index} 
                    onClick={ () => this.props.selectBook(book) }
                    className="list-group-item">
                    {book.title}
                </li>
            );
        });
    }
    
    render() {
        const books = this.props.books;
        return (
            <ul className="list-group col-sm-4">
                { this.renderList(books) }
            </ul>
        )
    }
}

function mapStateToProps(state) {
    // Whatever is returned will show up as props in BookList
    return {
        books: state.books
    }
}

//Anything returned from this will end up as props on BookList container
function mapDispatchToProps(dispatch) {
    // Whener selectBook is called, the result should be passed to all of our reducers
    return bindActionCreators({ selectBook: selectBook}, dispatch);
}

// Promote BookList from a component to a container - it needs to know about this new dispatch method selectBook. Make it available as a prop
export default connect(mapStateToProps,mapDispatchToProps)(BookList);